[![pipeline status](https://gitlab.inria.fr/auctus/panda/velocity_qp/badges/master/pipeline.svg)](https://gitlab.inria.fr/auctus/panda/velocity_qp)

# Panda qp control

A ROS package to control a Franka Emika panda robot using the Quadratic Programing controllers [velocity_qp](https://gitlab.inria.fr/auctus-team/components/control/velocity_qp) and [torque_qp](https://gitlab.inria.fr/auctus-team/components/control/torque_qp).

## Some necessary package to install first:

`sudo apt install python-rosdep python-catkin-tools python-wstool python-vcstool build-essential cmake git libpoco-dev libeigen3-dev`

## Install panda_qp_control
```terminal
mkdir -p ~/auctus_ws/dev_ros_ws/src
cd ~/auctus_ws/dev_ros_ws/src
git clone https://gitlab.inria.fr/auctus/panda/panda_qp_control.git
wstool init 
wstool merge panda_qp_control/panda_qp_control.rosinstall
wstool update
rosdep install --from-paths ../src --ignore-src -r -y
catkin config --init
catkin build
```

## Change environment variables
```
echo "source ~/auctus_ws/dev_ros_ws/devel/setup.bash" >> ~/.bashrc
source ~/.bashrc
```
# Usage

## In simulation 

For a velocity controller :
`roslaunch panda_qp_control velocity_control.launch sim:=true`

Note: The simulation of the robot in velocity is a bit hacky for now. It uses a EffortInterface. The joint velocity computed by the QP is fed to a proportionnal controller as $`k_p (\dot{q}^{opt} - \dot{q})`$. This is because for now the [franka_gazebo](https://frankaemika.github.io/docs/franka_ros.html#franka-gazebo) doesn't take into consideration a VelocityJointInterface. Hopefully this will be fixed with [https://github.com/frankaemika/franka_ros/pull/181](https://github.com/frankaemika/franka_ros/pull/181)

For a torque controller :
`roslaunch panda_qp_control torque_control.launch sim:=true`

## On the real robot

For a velocity controller :
`roslaunch velocity_qp velocity_control.launch robot_ip:=your_robot_ip`

Note: There is the same error as with the simulation. Therefore the robot is not stiffly actuated. This can be fixed by using a hardware_interface::VelocityJointInterface but it prevents simulating the robot.

For a torque controller :
`roslaunch panda_qp_control torque_control.launch robot_ip:=your_robot_ip`

## Run a trajectory

In this version of panda_qp_control, trajectories are computed using MoveIt. Several examples of trajectories are available in the package [moveit_trajectory_interface](https://gitlab.inria.fr/auctus-team/components/motion-planning/moveit_trajectory_interface)/test. 

To launch a trajectory simply run the command :
`rosrun moveit_trajectory_interface go_to_cartesian_pose.py`

MoveIt is used to allow a more high level definition of a trajectory than with [panda_traj](https://gitlab.inria.fr/auctus-team/components/control/panda_traj). MoveIt also include obstacle avoidance feature when planning for a motion. External sensors can also be added to sample the environment and take it into account durint the planning. The [moveit_trajectory_interface](https://gitlab.inria.fr/auctus-team/components/motion-planning/moveit_trajectory_interface) is a package that grabs a trajectory object planned by moveit and exposes the resulting trajectory as a function of time. A new point along the trajectory must be fed to the QP controller at each time step (1ms). More information on the trajectory generation can be found [here](https://gitlab.inria.fr/auctus-team/components/motion-planning/moveit_trajectory_interface))

If one wants to use the old way to generate trajectories, a branch with kdl_trajectories is available [here](https://gitlab.inria.fr/auctus-team/components/robots/panda/panda_qp_control/-/tree/kdl_trajectories)

## Optional roslaunch parameter : 

`robot_ip` IP adress of the panda robot

`sim` Run the code in simulation on Gazebo

`load_gripper` launch the panda model with its gripper

# Controller parameters

The controller parameters are stored in a yaml file in the `/config` folder and loaded as ros parameters in the `run.launch` file. 

